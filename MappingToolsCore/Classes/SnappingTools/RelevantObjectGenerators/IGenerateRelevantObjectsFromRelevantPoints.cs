﻿using System.Collections.Generic;

namespace Mapping_Tools.Classes.SnappingTools.RelevantObjectGenerators {

    public interface IGenerateRelevantObjectsFromRelevantPoints : IGenerateRelevantObjects {

        List<IRelevantObject> GetRelevantObjects(List<RelevantPoint> objects);
    }
}