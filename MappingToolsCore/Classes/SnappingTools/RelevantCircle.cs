﻿using Mapping_Tools.Classes.MathUtil;
using System;

namespace Mapping_Tools.Classes.SnappingTools {

    public class RelevantCircle : IRelevantObject {
        public readonly Circle child;

        public double DistanceTo(Vector2 point) {
            var dist = Vector2.Distance(point, child.Centre);
            return Math.Abs(dist - child.Radius);
        }

        public bool Intersection(IRelevantObject other, out Vector2[] intersections) {
            throw new NotImplementedException();
        }

        public Vector2 NearestPoint(Vector2 point) {
            var diff = point - child.Centre;
            var dist = diff.Length;
            return child.Centre + diff / dist * child.Radius;
        }

        public RelevantCircle(Circle circle) {
            child = circle;
        }
    }
}