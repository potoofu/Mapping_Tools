﻿using System.Windows;

namespace Mapping_Tools {

    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application {

        protected override void OnExit(ExitEventArgs e) {
            try {
            }
            finally {
                base.OnExit(e);
            }
        }
    }
}