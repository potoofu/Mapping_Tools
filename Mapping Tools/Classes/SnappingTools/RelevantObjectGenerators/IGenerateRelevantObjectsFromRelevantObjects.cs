﻿using System.Collections.Generic;

namespace Mapping_Tools.Classes.SnappingTools.RelevantObjectGenerators {

    public interface IGenerateRelevantObjectsFromRelevantObjects : IGenerateRelevantObjects {

        List<IRelevantObject> GetRelevantObjects(List<IRelevantObject> objects);
    }
}