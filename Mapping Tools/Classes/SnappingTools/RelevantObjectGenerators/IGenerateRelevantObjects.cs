﻿namespace Mapping_Tools.Classes.SnappingTools.RelevantObjectGenerators {

    public interface IGenerateRelevantObjects {
        bool IsActive { get; set; }
        string Name { get; }
        GeneratorType GeneratorType { get; }
    }
}