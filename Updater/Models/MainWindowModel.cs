﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Updater.Models {

    public class MainWindowModel : INotifyPropertyChanged {

        public MainWindowModel() {
            //Status = UpdaterStatus.CheckingUpdates;
            Manifest = new List<ManifestItem>();
            UpdateQueue = new Stack<ManifestItem>();
        }

        public UpdaterStatus Status {
            get {
                return Status;
            }
            set {
                Status = value;
                OnPropertyChanged("Status");
            }
        }

        public List<ManifestItem> Manifest {
            get {
                return Manifest;
            }
            set {
                Manifest = value;
                OnPropertyChanged("Manifest");
            }
        }

        public Stack<ManifestItem> UpdateQueue {
            get {
                return UpdateQueue;
            }
            set {
                UpdateQueue = value;
                OnPropertyChanged("UpdateQueue");
            }
        }

        public enum UpdaterStatus {
            Idle,
            CheckingUpdates,
            HasUpdates,
            ReadyToUpdate,
            Downloading,
            Finished,
            NoUpdates,
            CommonError,
            NoInternet,
            DownloadError,
            NoAccess,
            NoAccessDelete
        }

        public struct ManifestItem {
            public Uri ServerPath { get; set; }
            public Uri LocalPath { get; set; }
            public string Hash { get; set; }
            public string Name { get; set; }
            public bool IsFile { get; set; }
        }

        public static readonly string Domain = "https://mappingtools.seira.moe";
        public static readonly string ExecutionPath = AppDomain.CurrentDomain.BaseDirectory;
        public static readonly string ServerPathToRemove = "/srv/mappingtools/backend/data";
        public static readonly string LocalPathToRemove = "/updater/resources";
        public static readonly string ManifestPath = "https://mappingtools.seira.moe/updater/manifest.json";
        public static readonly string UpdateJsonPath = "https://mappingtools.seira.moe/current/updater.json";

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propName) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }
}