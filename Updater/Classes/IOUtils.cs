﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using Updater.Classes.Exceptions;
using static Updater.Models.MainWindowModel;

namespace Updater.Classes.IO {

    public abstract class IOUtils {

        public static void AdjustDirectories(List<ManifestItem> manifest) {
            try {
                foreach (var item in manifest) {
                    if (!item.IsFile) {
                        Directory.CreateDirectory(item.LocalPath.LocalPath);
                    }
                }
            }
            catch (UnauthorizedAccessException) {
                throw new UnauthorizedAccessException();
            }
            catch (Exception) {
                throw new Exception();
            }
        }

        public static async void DownloadFileToSystem(string webPath, string localPath) {
            try {
                using var client = new WebClient();
                await client.DownloadFileTaskAsync(webPath, localPath);
            }
            catch (WebException) {
                throw new WebException();
            }
            catch (Exception) {
                throw new DownloadException();
            }
        }

        public static bool TryGetMD5HashFromFile(string localPath, out string hash) {
            try {
                if (File.Exists(localPath)) {
                    using var md5 = MD5.Create();
                    using var fileStream = File.OpenRead(localPath);
                    var md5Hash = md5.ComputeHash(fileStream);
                    hash = BitConverter.ToString(md5Hash).Replace("-", "").ToLowerInvariant();
                    return true;
                }
                else {
                    hash = null;
                    return false;
                }
            }
            catch (UnauthorizedAccessException) {
                throw new UnauthorizedAccessException();
            }
            catch (Exception) {
                throw new MD5HashException();
            }
        }

        public static void OpenBrowser(string url) {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) {
                Process.Start(new ProcessStartInfo("cmd", $"/c start {url}"));
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux)) {
                Process.Start("xdg-open", url);
            }
            else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX)) {
                Process.Start("open", url);
            }
        }
    }
}