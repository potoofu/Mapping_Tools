﻿using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Updater.Classes.Exceptions;
using Updater.Classes.IO;
using Updater.Classes.Web;
using Updater.Models;
using static Updater.Models.MainWindowModel;

namespace Updater.ViewModels {

    public class MainWindowViewModel {
        public MainWindowModel Model { get; set; }

        public MainWindowViewModel() {
            Model = new MainWindowModel {
                Status = UpdaterStatus.Idle
            };
        }

        /*
        private Task<bool> Update() {
            return true;
        }
        */

        private async Task<bool> CheckForUpdate() {
            JObject informationJObject;
            try {
                informationJObject = await Webutils.GetJSON(UpdateJsonPath);
            }
            catch (GetJSONException) {
                throw new GetJSONException();
            }

            if (Version.TryParse((string)informationJObject["version"], out var version)) {
                var currentAssembly = Assembly.GetEntryAssembly().GetName().Version;
                return currentAssembly.CompareTo(version) < 0;
            }
            else {
                throw new VersionParseException();
            }
        }

        private async void PrepareForUpdate() {
            JObject manifestJObject;
            try {
                manifestJObject = await Webutils.GetJSON(ManifestPath);
                SetManifestFromJSON(manifestJObject);
                AnalyzeManifest();
                //CleanupFiles(ExecutionPath);
            }
            catch (GetJSONException) {
                throw new GetJSONException();
            }
            catch (PathParseException) {
                throw new PathParseException();
            }
            catch (UriParseException) {
                throw new UriParseException();
            }
        }

        private void SetManifestFromJSON(JObject manifestJObject) {
            Model.Manifest.Clear();
            foreach (var item in (JArray)manifestJObject["paths"]) {
                string path, serverPath, localPath;
                try {
                    path = ((string)item["path"]).Replace(ServerPathToRemove, "");
                    serverPath = Domain + path;
                    localPath = path.Replace(LocalPathToRemove, "");
                    localPath = localPath.Remove(0, 1);
                    localPath = Path.Combine(ExecutionPath, localPath);
                }
                catch (PathParseException) {
                    throw new PathParseException();
                }

                if (Uri.TryCreate(serverPath, UriKind.Absolute, out var serverPathUri)) {
                    if (Uri.TryCreate(localPath, UriKind.Absolute, out var localPathUri)) {
                        var name = localPath.Split(Path.DirectorySeparatorChar).Last();
                        Model.Manifest.Add(new ManifestItem {
                            ServerPath = serverPathUri,
                            LocalPath = localPathUri,
                            Hash = (string)item["hash"],
                            Name = name,
                            IsFile = (bool)item["isFile"]
                        });
                    }
                    else {
                        throw new UriParseException();
                    }
                }
                else {
                    throw new UriParseException();
                }
            }
        }

        private void AnalyzeManifest() {
            foreach (var item in Model.Manifest) {
                if (item.IsFile) {
                    try {
                        if (IOUtils.TryGetMD5HashFromFile(item.LocalPath.LocalPath, out var hash)) {
                            if (!hash.Equals(item.Hash)) {
                                Model.UpdateQueue.Push(item);
                            }
                        }
                        else {
                            throw new MD5HashException();
                        }
                    }
                    catch (UnauthorizedAccessException) {
                        throw new UnauthorizedAccessException();
                    }
                    catch (MD5HashException) {
                        throw new MD5HashException();
                    }
                }
            }
        }

        private void CleanupFiles(string dirPath) {
            try {
                var files = Directory.GetFiles(dirPath);
                foreach (var filePath in files) {
                    if (!Model.Manifest.Exists(s => s.LocalPath.LocalPath == filePath)) {
                        File.Delete(filePath);
                    }
                }

                var dirs = Directory.GetDirectories(dirPath);
                if (dirs.Length > 0) {
                    foreach (var dir in dirs) {
                        CleanupFiles(dir);
                    }
                }
                else {
                    if (!Model.Manifest.Exists(s => s.LocalPath.LocalPath == dirPath)) {
                        if (Directory.GetFiles(dirPath).Length == 0 && Directory.GetDirectories(dirPath).Length == 0) {
                            Directory.Delete(dirPath);
                        }
                    }
                }
            }
            catch (UnauthorizedAccessException) {
                throw new UnauthorizedAccessException();
            }
            catch (Exception) {
                throw new CleanupException();
            }
        }
    }
}